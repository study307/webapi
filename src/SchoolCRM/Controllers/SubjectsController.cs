﻿using Microsoft.AspNetCore.Mvc;
using SchoolCRM.Services;


namespace SchoolCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubjectsController : Controller
    {
        INameResolver _nameResolver = null;
        public SubjectsController(INameResolver nameResolver)
        {
            this._nameResolver = nameResolver;
        }

        [HttpGet]
        public string Get()
        {
            return _nameResolver.Resolve(this);
        }
        
    }
}
