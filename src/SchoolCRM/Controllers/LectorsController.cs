﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolCRM.Services;

namespace SchoolCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LectorsController : Controller
    {
        INameResolver _nameResolver = null;
        public LectorsController(INameResolver nameResolver)
        {
            this._nameResolver = nameResolver;
        }                      

        [HttpGet]
        public string Get()
        {
            return _nameResolver.Resolve(this);            
        }
        
    }
}
