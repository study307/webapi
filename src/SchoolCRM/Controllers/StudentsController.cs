﻿using Microsoft.AspNetCore.Mvc;
using SchoolCRM.Services;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SchoolCRM.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        INameResolver _nameResolver = null;
        public StudentsController(INameResolver nameResolver)
        {
            this._nameResolver = nameResolver;
        }

        [HttpGet]
        public string Get()
        {
            return _nameResolver.Resolve(this);
        }

    }
}
