﻿using Microsoft.Extensions.Options;
using SchoolCRM.Services;

internal class ObjectTypeResolver : INameResolver
{
    private readonly ILogger _logger;
    private readonly IOptionsMonitor<NameTypeResolverOptions> _options;

    public ObjectTypeResolver(ILogger<ObjectTypeResolver> logger, IOptionsMonitor<NameTypeResolverOptions> options)
    {
        _options = options;    
        _logger = logger;   
    }

    

    public string Resolve(object obj)
    {
        _logger.LogWarning("This is warning");
        _logger.LogInformation("Method Resolve starts");        
        var nameControllerType = obj.GetType();
        _logger.LogDebug($"ObjType = {nameControllerType} resolved name {nameControllerType.Name}");                     

        if (_options.CurrentValue.Prefix != null && _options.CurrentValue.Suffix != null)
        {
            return string.Concat(_options.CurrentValue.Prefix, nameControllerType.Name, _options.CurrentValue.Suffix);               
        }
        if (_options.CurrentValue.Prefix == null && _options.CurrentValue.Suffix != null)
        {
            return string.Concat(nameControllerType.Name, _options.CurrentValue.Suffix);
        }
        if (_options.CurrentValue.Prefix != null && _options.CurrentValue.Suffix == null)
        {
            return string.Concat(_options.CurrentValue.Prefix, nameControllerType.Name);
        }                 
            return nameControllerType.Name;        
        
    }
}
