﻿namespace SchoolCRM.Services
{
    public interface INameResolver
    {
        string Resolve(object obj); 
    }
}
