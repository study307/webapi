﻿namespace SchoolCRM.Services
{
    public class NameTypeResolverOptions
    {
        public const string NameTypeResolver = "NameTypeResolver";
        public string Prefix { get; set; } = String.Empty;
        public string Suffix { get; set; } = String.Empty;
    }
}
